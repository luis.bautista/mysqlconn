<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="vendor/bootstrap-4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Connection to remote MySQL Server</title>
</head>
<body>
    <?php
        include('conn.php');
        $stm = $pdo->query("SELECT * FROM users;");
    ?>

    <table class="table table-striped">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">User Name</th>
            <th scope="col">Password</th>
            </tr>
        </thead>
        <tbody>
        <?php
            while ($user = $stm->fetch(PDO::FETCH_ASSOC)) {
                echo "<tr>";
                echo "<td>" . $user['user_id'] . "</td>";
                echo "<td>" . $user['name'] . "</td>";
                echo "<td>" . $user['password'] . "</td>";
                echo "</tr>";
            }   
        ?>
        </tbody>
    </table>
</body>
</html>

